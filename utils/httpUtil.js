const globalCache = require("./cache.js");
const API = require("./api.js");

//apiName： 接口URL
//data：請求參數
//method: 请求方式，值为：GET或POST
//callBackFunc：回調函數 （請求方法本身不做任務處理，只是調用接口然後返回數據，所有邏輯都在回調方法中自己處理）
//that 調用接口的頁面本身（xxx.js）
function callApi(apiName, data, method, callBackFunc, that, contentType, loadingMsg) {
  var loadingMsg = loadingMsg ? loadingMsg : '加载中...';
  wx.showLoading({
    title: loadingMsg,
    mask: true
  })
  wx.request({
    header: {
      'content-type': contentType,
      'X-Long-Token': globalCache.getUserToken(),
      'X-Open-Id': globalCache.getOpenId(),
      'X-Union-Id': globalCache.getUnionId(),
      'X-User-Type': globalCache.getUserType()
      // 'content-type': 'application/json'
    },
    method: method,
    url: API.getApi(apiName),
    data: data,
    success: function (res) {
      wx.hideLoading();
      var data = res.data;
      if (isSuccess(data)) {
        callBackFunc(data, that);
      } else {
        console.error("API[" + apiName + "] return a error, code: " + data.code + ", msg: " + data.msg);
        wx.showToast({
          title: data.msg,
          icon: 'none',
        });
      }
    },
    fail: function (res) {
      wx.hideLoading();
      console.error("failed to call API: " + apiName + ", cause: " + res);
      wx.showToast({
        title: '网络异常',
        icon: 'none',
      });
    },
    complete: function () {
      // complete
    }
  })
}

//GET 请求方法
//apiName： 接口URL
//data：請求參數
//callBackFunc：回調函數 （請求方法本身不做任務處理，只是調用接口然後返回數據，所有邏輯都在回調方法中自己處理）
//that 調用接口的頁面本身（xxx.js）
function requestGet(apiName, data, callBackFunc, that, loadingMsg) {
  callApi(apiName, data, 'GET', callBackFunc, that, 'application/x-www-form-urlencoded', loadingMsg);
}

//POST 请求方法
//apiName： 接口URL
//data：請求參數
//callBackFunc：回調函數 （請求方法本身不做任務處理，只是調用接口然後返回數據，所有邏輯都在回調方法中自己處理）
//that 調用接口的頁面本身（xxx.js）
function requestPost(apiName, data, callBackFunc, that, loadingMsg) {
  callApi(apiName, JSON.stringify(data), 'POST', callBackFunc, that, 'application/json', loadingMsg);
}

//判断API请求是否正常
function isSuccess(res) {
  if(res.code == 0) {
    return true;
  }
}

function uploadFile(apiName, data, callBackFunc, that) {
  var i = data.i ? data.i : 0;    //当前上传的哪张图片
  var success = data.success ? data.success : 0; //上传成功的个数
  var fail = data.fail ? data.fail : 0;  //上传失败的个数
  wx.uploadFile({
    url: API.getApi(apiName),
    filePath: data.path[i],
    name: 'file',//这里根据自己的实际情况改
    success: (res) => {
      var resData = JSON.parse(res.data);
      if (isSuccess(resData)) {
        success++;//图片上传成功，图片上传成功的变量+1
        globalCache.setUploadFileUrl(resData.data);
      } else {
        fail++;//图片上传失败，图片上传失败的变量+1
      }
    },
    fail: (res) => {
      fail++;//图片上传失败，图片上传失败的变量+1
    },
    complete: () => {
      i++;//这个图片执行完上传后，开始上传下一张
      if (i == data.path.length) {   //当图片传完时，停止调用          
        callBackFunc(null, that);
      } else {//若图片还没有传完，则继续调用函数
        data.i = i;
        data.success = success;
        data.fail = fail;
        uploadFile(apiName, data, callBackFunc, that);
      }
    }
  });
}

//导出各方法
module.exports = {
  requestGet: requestGet,
  requestPost: requestPost,
  uploadFile: uploadFile
}