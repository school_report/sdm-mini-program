//公共緩存類

//设置全局KEY
function setCache(key, val) {
  wx.setStorage({
    key: key,
    data: val,
    success: function (res) {
      return true;
    },
    fail: function () {
      // fail
    },
    complete: function () {
      // complete
    }
  })
}

function setCacheSync(key, val) {
  wx.setStorageSync(key, val);
}

//获取全局KEY
function getCache(key) {
  return wx.getStorageSync(key);
}

function setUserInfo(userInfo) {
  setCache('user-info', userInfo);
}

function getUserInfo() {
  return wx.getStorageSync('user-info');
}

//获取用户ID
function getUserId() {
  var userInfo = getUserInfo();
  var id = 0;
  if (userInfo != null && userInfo.id != null) {
    id = userInfo.id;
  }
  return id;
}

//获取用户Token
function getUserToken() {
  var userInfo = getUserInfo();
  var token = "";
  if (userInfo != null && userInfo.token != null) {
    token = userInfo.token;
  }
  return token;
}

//获取用户OpenId
function getOpenId() {
  var userInfo = getUserInfo();
  var openId = "";
  if (userInfo != null && userInfo.openId != null) {
    openId = userInfo.openId;
  }
  return openId;
}

//获取用户UnionId
function getUnionId() {
  var userInfo = getUserInfo();
  var unionId = "";
  if (userInfo != null && userInfo.unionId != null) {
    unionId = userInfo.unionId;
  }
  return unionId;
}

function setUserType(userType) { //1:家长; 2:老师
  setCache('user-Type', userType);
}

function getUserType() {//1:家长; 2:老师
  return wx.getStorageSync('user-Type');
}

function isLogin() {
  var userInfo = getUserInfo();
  if (userInfo != null && userInfo.id != null) {
    return true;
  } else {
    return false;
  }
}

function setCurrentChildId(childId) {
  setCache('child-id', childId);
}

function getCurrentChildId() {
  return wx.getStorageSync('child-id');
}

function setCurrentChildName(childName) {
  setCache('child-name', childName);
}

function getCurrentChildName() {
  return wx.getStorageSync('child-name');
}

function setCurrentTeamId(classId) {
  setCache('team-id', classId);
}

function getCurrentTeamId() {
  return wx.getStorageSync('team-id');
}

function setUploadFileUrl(url) {
  var fileUrls = wx.getStorageSync('file-url');
  fileUrls.push(url);
  setCacheSync('file-url', fileUrls);
}

function retsetUploadFileUrl() {
  setCacheSync('file-url', []);
}

function getUploadFileUrl() {
  return wx.getStorageSync('file-url');
}

function removeFileUrl() {
  wx.removeStorage({
    key: 'file-url',
    success: function(res) {},
  })
}

module.exports = {
  setCache: setCache,
  getCache: getCache,
  setUserInfo: setUserInfo,
  getUserInfo: getUserInfo,
  getUserToken: getUserToken,
  getOpenId: getOpenId,
  getUnionId: getUnionId,
  setUserType: setUserType,
  getUserType: getUserType,
  isLogin: isLogin,
  getUserId: getUserId,
  setCurrentChildId: setCurrentChildId,
  getCurrentChildId: getCurrentChildId,
  setCurrentTeamId: setCurrentTeamId,
  getCurrentTeamId: getCurrentTeamId,
  retsetUploadFileUrl: retsetUploadFileUrl,
  setUploadFileUrl: setUploadFileUrl,
  getUploadFileUrl: getUploadFileUrl,
  removeFileUrl: removeFileUrl,
  setCurrentChildName: setCurrentChildName,
  getCurrentChildName: getCurrentChildName
}