var isTest = false; 
//测试环境 
var testDomain = "http://10.19.12.241:8080";
//正式环境
var prodDomain = "https://api.kejian18.com";
let httpDomain = isTest ? testDomain : prodDomain;

var API = {
  authorize: "/we-chat/m/authorize",
  userLogin: "/we-chat/m/login",
  childList: "/child/list",
  modifyUserInfo: "/user/modify_info",
  sendVerifyCode: "/user/send_code",
  bindChild: "/user/bind",
  uploadFile: "/common/upload",
  isAccountLogin: "/common/useAccountLogin",
  publishNews: "/news/publish",
  newsList: "/news/list",
  deleteNews: "/news/delete",
  childScoreList: "/score/listByChildId",
  teamList: "/team/listByUserId",
  activityList: "/activity/list",
  addActivity: "/activity/add",
  deleteActivity: "/activity/delete",
  accountLogin: "/user/accountLogin",
}

function getApi(apiName) {
  return httpDomain + API[apiName];
}

//注册模块
module.exports = {
  getApi: getApi
}