
//获取地址信息
function getCation(callBackFunc, that) {
  wx.getLocation({
    type: 'wgs84',
    success: function (res) {
      callBackFunc(res, that);
    }
  });
}

//获取地址信息
function chooseLocation(callBackFunc, that) {
  wx.chooseLocation({
    type: 'wgs84',
    success: function (res) {
      callBackFunc(res, that);
    }
  });
}



module.exports = {
  getCation: getCation,
  chooseLocation: chooseLocation,
  city: '',
  latitude: 0,
  longitude: 0,
  baiduAK: 'Y1R5guY8Y2GNRdDpLz7SUeM3QgADAXec',
  apiList: {
    baiduMap: 'https://api.map.baidu.com/geocoder/v2/'
  },
}