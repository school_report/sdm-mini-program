//app.js
const httpUtil = require("./utils/httpUtil.js");
const globalCache = require("./utils/cache.js");

App({
  onLaunch: function () {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)

    // 登录
    var that = this;
    wx.login({
      success: res => {
        httpUtil.requestPost("authorize", {code: res.code}, function(res, that) {
          console.log("successfully to authorize, openId: " + res.data.openId);
          globalCache.setUserInfo(res.data);
        }, that);
      }
    })
  }
})