// pages/bind/bind.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");
const util = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showCountDown: false,
    coundDownTime: 59,
    phoneNumber: '',
    intervalNumber: 0,
    tips: "输入您家小朋友在班主任那备案的手机号，如没有备案，请先联系班主任进行备案",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '绑定账号'
    })

    var userType = globalCache.getUserType();
    if (userType == 2) {//老师
      this.setData({
        tips: "输入您备案的手机号，如没有备案，请先联系班主任进行备案",
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(this.data.intervalNumber);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getPhone: function (e) {
    this.setData({
      phoneNumber: e.detail.value
    })
  },

  /**
  * 获取验证码
  */
  getVerifyCode: function () {
    if (util.isEmpty(this.data.phoneNumber)) {
      wx.showToast({
        title: '请输入手机号码',
        icon: 'none',
      });
      return;
    }
    var that = this;
    var requestData = new Object();
    requestData.mobile = that.data.phoneNumber;
    requestData.type = "bind";
    requestData.userType = globalCache.getUserType();
    httpUtil.requestPost("sendVerifyCode", requestData, function (res, that) {
      console.log("successfully send code to suer");
      wx.showToast({
        title: '验证码发送成功',
      });
      that.setData({
        showCountDown: true,
        coundDownTime: 59
      });
      that.countDown();
    }, that, "发送中...");
  },

  countDown: function () {
    var that = this;
    var countDown = 59;
    var timer = setInterval(function () {
      countDown--;
      that.setData({
        coundDownTime: countDown
      })
      if (countDown < 1) {
        that.setData({
          showCountDown: false,
          coundDownTime: 59
        })
        clearInterval(that.data.intervalNumber);
      } 
    }, 1000);
    that.setData({
      intervalNumber: timer,
    })
  },

  /**
   * 提交绑定数据
   */
  submitBind: function (e) {
    var that = this;
    var bindData = new Object();
    bindData.mobile = e.detail.value.mobile;
    bindData.userType = globalCache.getUserType();
    bindData.type = "bind";
    bindData.verifyCode = e.detail.value.verifyCode;
    if (!that.checkInputValue(bindData)) {
      return;
    }

    httpUtil.requestPost("bindChild", bindData, function (res, that) {
      console.log("successfully bind child for user");
      var userInfo = globalCache.getUserInfo();
      if (globalCache.getUserType() == 1) {//家长
        userInfo.guardianId = res.data.guardianId;
      } else {//老师
        userInfo.teacherId = res.data.teacherId
      }
      globalCache.setUserInfo(userInfo);
      wx.showToast({
        title: '绑定成功',
        duration: 1000,
      });
      var timeNumber = setTimeout(function () {
        clearTimeout(timeNumber);
        wx.navigateBack({
          delta: 2
        });
      }, 1000);
      
    }, that, "提交中...");
  },

  checkInputValue: function (bindData) {
    if (util.isEmpty(bindData.mobile)) {
      wx.showToast({
        title: '请输入手机号码',
        icon: 'none',
      });
      return false;
    }
    if (util.isEmpty(bindData.verifyCode)) {
      wx.showToast({
        title: '请输入验证码',
        icon: 'none',
      });
      return false;
    }
    return true;
  }
})