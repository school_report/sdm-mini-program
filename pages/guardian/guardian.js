// pages/guardian/guardian.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");
const utils = require("../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
      'https://img.jiesuyx.com/p/1545118604493.jpg'
    ],
    indicatorDots: false,
    autoplay: true,
    interval: 5000,
    duration: 1000
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 用webview打开二级页面
   */
  openH5: function (event) {
    var url = event.currentTarget.dataset['url'];
    if (!utils.isEmpty(url)) {
      wx.navigateTo({
        url: '../h5/h5?url=' + url,
      })
    }
  },

  showNews: function (event) {
    var newsType = event.currentTarget.dataset['type'];
    wx.navigateTo({
      url: '../teamNews/teamNews?newsType=' + newsType
    })
  },

  showScore: function (event) {
    wx.navigateTo({
      url: '../score/score'
    })
  },

  showActivity: function () {
    wx.navigateTo({
      url: '../activity/activity'
    })
  }
})