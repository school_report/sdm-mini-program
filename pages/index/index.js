//index.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");

//获取应用实例
const app = getApp()
Page({

  data: {
    availabeHeight: "0px"
  },

  //事件处理函数
  setUserType: function(event) {
    if(globalCache.isLogin()) {
      var userType = event.currentTarget.dataset['type'];
      globalCache.setUserType(userType);
      if(userType == 1) {//家长入口
        this.childList(userType);
      } else {//老师入口
        this.teamList(userType);
      }
    } else {//跳转到登录页
      wx.navigateTo({
        url: '../login/login'
      })
    }
  },

  childList: function () {
    console.log('click guardian button...');
    var that = this;
    httpUtil.requestGet("childList", { userId: globalCache.getUserId() }, function (res, that) {
      var childList = res.data;
      if(childList.length <= 0) {
        console.warn("not found any children");
        wx.navigateTo({
          url: '../nobind/nobind',
        })
      } else if (childList.length > 1) {
        wx.navigateTo({
          url: '../childList/childList',
        })
      } else { 
        globalCache.setCurrentChildId(childList[0].id);
        globalCache.setCurrentChildName(childList[0].name);
        globalCache.setCurrentTeamId(childList[0].teamId);
        wx.navigateTo({
          url: '../guardian/guardian',
        })
      }
    }, that);
  },

  teamList: function () {
    console.log('click teacher button...');
    var that = this;
    httpUtil.requestGet("teamList", { userId: globalCache.getUserId() }, function (res, that) {
      var teamList = res.data;
      if (teamList.length <= 0) {
        console.warn("not found any team info");
        wx.navigateTo({
          url: '../nobind/nobind',
        })
      } else if (teamList.length > 1) {
        wx.navigateTo({
          url: '../teamList/teamList',
        })
      } else {
        globalCache.setCurrentTeamId(teamList[0].teamId);
        wx.navigateTo({
          url: '../teacher/teacher',
        })
      }
    }, that);
  },

  onLoad: function () {
    var that = this;
    //动态设置入口区域的高度
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          availabeHeight: (res.windowHeight - 32) + "px"
        })
      }
    })
  }

})
