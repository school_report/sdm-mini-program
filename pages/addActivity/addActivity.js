// pages/addActivity/addActivity.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");
const util = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '',
    content: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  getTitle: function (e) {
    this.setData({
      title: e.detail.value
    })
  },

  getContent: function (e) {
    this.setData({
      content: e.detail.value
    })
  },

  submitData: function () {
    if (util.isEmpty(this.data.title)) {
      wx.showToast({
        title: '公告标题不能为空!',
        icon: 'none'
      });
      return;
    }
    if (util.isEmpty(this.data.content)) {
      wx.showToast({
        title: '公告内容不能为空!',
        icon: 'none'
      });
      return;
    }

    var that = this;
    var data = new Object();
    data.title = that.data.title
    data.content = that.data.content;
    data.teamId = globalCache.getCurrentTeamId();
    data.userId = globalCache.getUserId();
    httpUtil.requestPost("addActivity", data, function (res, that) {
      wx.navigateBack({
        delta: 1
      });
    }, that, "提交中...");
  }
})