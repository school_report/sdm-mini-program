// pages/my/my.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '../../images/ic_default_avatar.png',
    userName: '点击登录',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '我'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var userInfo = globalCache.getUserInfo();
    if (userInfo != null && userInfo.avatarUrl != null) {
        this.setData({
          avatarUrl: userInfo.avatarUrl,
          userName: userInfo.name
        })
    } else if (userInfo != null && userInfo.token !=null && userInfo.avatarUrl == null) {
      this.setData({
        userName: "获取用户名及头像"
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
  /**
   * 跳转到登录页面
   */
  goToLogin: function () {
    var userInfo = globalCache.getUserInfo();
    if (userInfo.testAccount) {
      return;
    }
    if (userInfo == null || userInfo.token == null) {
      wx.navigateTo({
        url: '../login/login',
      })
    } else if (userInfo.avatarUrl == null) {
      wx.navigateTo({
        url: '../profile/profile',
      })
    }
  },

  /**
   * 小朋友列表页面
   */
  childList: function () {
    if (globalCache.isLogin()) {
      var userInfo = globalCache.getUserInfo();
      if (userInfo.guardianId != null && userInfo.guardianId > 0) {
        wx.navigateTo({
          url: '../childList/childList',
        })
      } else {
        wx.showToast({
          title: '您没有权限查看此信息！',
          icon: 'none'
        })
      }
    } else {
      wx.navigateTo({
        url: '../login/login',
      })
    }
  },

  teamList: function () {
    if (globalCache.isLogin()) {
      var userInfo = globalCache.getUserInfo();
      if (userInfo.teacherId != null && userInfo.teacherId > 0) {
        wx.navigateTo({
          url: '../teamList/teamList',
        })
      } else {
        wx.showToast({
          title: '您没有权限查看此信息！',
          icon: 'none'
        })
      }
    } else {
        wx.navigateTo({
          url: '../login/login',
        })
    }
  },

  /**
   * 关于我们页面
   */
  aboutUs: function () {
    wx.navigateTo({
      url: '../about/about',
    })
  }
})