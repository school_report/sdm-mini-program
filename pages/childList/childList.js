// pages/selectChild/childList.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    childList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '小朋友列表'
    });

    var that = this;
    httpUtil.requestGet("childList", { userId: globalCache.getUserId() }, function (res, that) {
      that.setData({
        childList: res.data
      })
    }, that);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  showGradeInfo: function (event) {
    var childId = event.currentTarget.dataset['childId'];
    var teamId = event.currentTarget.dataset['teamId'];
    var childName = event.currentTarget.dataset['childName'];
    globalCache.setCurrentChildId(childId);
    globalCache.setCurrentTeamId(teamId);
    globalCache.setCurrentChildName(childName);
    wx.navigateTo({
      url: '../guardian/guardian',
    })
  }
})