// pages/login/login.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showAccountLogin: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '登录'
    })

    this.checkAccountLogin();
  },

  checkAccountLogin: function () {
    console.log('click guardian button...');
    var that = this;
    httpUtil.requestGet("isAccountLogin", {}, function (res, that) {
      if (res.data == true) {
        that.setData({
          showAccountLogin: true
        })
      }
    }, that);
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getPhoneNumber: function(e) {
    var result = e.detail.errMsg;
    console.warn("get user phone number, result: " + result);
    if (result == 'getPhoneNumber:ok') {
      var that = this;
      httpUtil.requestPost("userLogin", {encryptedData: e.detail.encryptedData, iv: e.detail.iv}, function (res, that) {
        console.log("user login successfully");
        var userInfo = res.data;
        globalCache.setUserInfo(userInfo);
        if (userInfo.name == null) {
          wx.navigateTo({
            url: '../profile/profile'
          })
        } else {
          wx.navigateBack({
            delta: 1
          })
        }
      }, that, "提交中...");
    } 
  },

  /**
   * 已有账号登录
   */
  goToLogin: function () {
    wx.navigateTo({
      url: '../account/account'
    })
  }
  
})