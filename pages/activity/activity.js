// pages/activity/activity.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");
//const utils = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    rows: 10,
    hasActivity: true,
    showAddButton: true,
    top: 0,
    left: 0,
    showHeight: 0,
    showWidth: 0,
    windowHeight: 0,
    windowWidth: 0,
    activityList: [],
    autoRefresh: false,
    currentUserId: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '学校公告',
    });

    this.initData();
    this.getActivityList();
  },

  /**
   * 初始化悬浮按钮位置
   */
  initData: function () {
    var userType = globalCache.getUserType();
    var showAddButton = true;
    if (userType == 1) {//家长
      showAddButton = false;
    }
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          top: res.windowHeight - 150,
          left: res.windowWidth - 100,
          showHeight: res.windowHeight - 150,
          showWidth: res.windowWidth - 100,
          windowHeight: res.windowHeight,
          windowWidth: res.windowWidth,
          showAddButton: showAddButton,
          currentUserId: globalCache.getUserId()
        })
      }
    })
  },

  /**
   * 获取学校公告列表
   */
  getActivityList: function () {
    var data = new Object();
    data.teamId = globalCache.getCurrentTeamId();
    data.page = this.data.page;
    data.rows = this.data.rows;
    var that = this;
    httpUtil.requestGet("activityList", data, function (res, that) {
      if (that.data.page == 1) {
        that.setData({
          activityList: []
        })
      }

      if (res.data.length > 0) {
        var activityList = that.data.activityList.concat(res.data);
        that.setData({
          hasActivity: true,
          activityList: activityList
        })
      }

      if (that.data.activityList.length <= 0) {
        that.setData({
          hasActivity: false,
        })
      }
    }, that, "加载中...");
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.autoRefresh) {
      this.setData({
        autoRefresh: false
      })
      wx.startPullDownRefresh();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
  * 页面相关事件处理函数--监听用户下拉动作
  */
  onPullDownRefresh: function () {
    this.setData({
      page: 1
    })
    this.getActivityList();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '玩命加载中',
    })
    var page = this.data.page + 1;
    this.setData({
      page: page
    })
    this.getActivityList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 新增学校公告
   */
  addActivity: function () {
    this.setData({
      autoRefresh: true
    })
    wx.navigateTo({
      url: '../addActivity/addActivity'
    })
  },

  detailInfo: function(event) {
    console.log("-----------");
  },

  deleteActivity: function(event) {
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          var activityId = event.currentTarget.dataset.id;
          var index = event.currentTarget.dataset.index;
          httpUtil.requestPost("deleteActivity", { activityId: activityId }, function (res, that) {
            var activityList = that.data.activityList;
            activityList.splice(index, 1);
            var hasActivity = true;
            if (activityList.length <= 0) {
              hasActivity = false;
            }
            that.setData({
              activityList: activityList,
              hasActivity: hasActivity
            });

            wx.showToast({
              title: '成功删除',
            });
          }, that, "提交中...");
        }
      }
    })
  },

  setTouchMove: function (e) {
    if (e.touches[0].clientX < this.data.showWidth && e.touches[0].clientY < this.data.showHeight
      && e.touches[0].clientX > 0 && e.touches[0].clientY > 0) {
      this.setData({
        left: e.touches[0].clientX,
        top: e.touches[0].clientY
      })
    } else {
      this.setData({
        left: this.data.showWidth,
        top: this.data.showHeight
      })
    }
  }
})