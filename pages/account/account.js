// pages/account/account.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");
const util = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '登录'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 提交绑定数据
   */
  submitBind: function (e) {
    var that = this;
    var bindData = new Object();
    bindData.mobile = e.detail.value.mobile;
    bindData.verifyCode = e.detail.value.verifyCode;
    if (!that.checkInputValue(bindData)) {
      return;
    }

    httpUtil.requestPost("accountLogin", bindData, function (res, that) {
      console.log("successfully bind child for user");
      var userInfo = res.data;
      globalCache.setUserInfo(userInfo);
      wx.showToast({
        title: '登录成功',
        duration: 1000,
      });
      wx.navigateBack({
        delta: 2
      });
    }, that, "提交中...");
  },

  checkInputValue: function (bindData) {
    if (util.isEmpty(bindData.mobile)) {
      wx.showToast({
        title: '请输入手机号码',
        icon: 'none',
      });
      return false;
    }
    if (util.isEmpty(bindData.verifyCode)) {
      wx.showToast({
        title: '请输入密码',
        icon: 'none',
      });
      return false;
    }
    return true;
  }
})