// pages/classNews/classNews.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");
//const utils = require("../../utils/util.js");

Page({
  /**
   * 页面的初始数据
   */
  data: {
    newsType: 1,
    page: 1,
    rows: 10,
    top: 0,
    left: 0,
    showHeight: 0,
    showWidth: 0,
    windowHeight: 0,
    windowWidth: 0,
    imageStyle: 'news-image',
    defaultUrl: '../../images/ic_default_avatar.png',
    newsList: [],
    showAddButton: false,
    hasNews: true,
    currentUserId: 0,
    autoRefresh: false,
    hasNoNewsTips: '还没有发布动态消息哦~',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var newsType = options.newsType;
    this.setData({
      newsType: newsType
    });

    this.setTitle();
    this.initData();
    this.getNewsList();
  },

  /**
   * 根据newsType类型设置不同的标题
   */
  setTitle: function () {
    var title = '班级动态'
    var hasNoNewsTips = '还没有发布动态消息哦~';
    if (this.data.newsType == "2") {
      title = "家庭作业";
      hasNoNewsTips = '还没有发布作业哦~';
    }
    this.setData({
      hasNoNewsTips: hasNoNewsTips
    })
    wx.setNavigationBarTitle({
      title: title,
    });
  },
  
  /**
   * 初始化悬浮按钮位置
   */
  initData: function () {
    var userType = globalCache.getUserType();
    var showAddButton = true;
    if (userType == 1) {//家长
      showAddButton = false;
    }
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          top: res.windowHeight - 150,
          left: res.windowWidth - 100,
          showHeight: res.windowHeight - 150,
          showWidth: res.windowWidth - 100,
          windowHeight: res.windowHeight,
          windowWidth: res.windowWidth,
          showAddButton: showAddButton,
          currentUserId: globalCache.getUserId()
        })
      }
    })
  },

  /**
   * 获取新闻/家庭作业列表
   */
  getNewsList: function () {
    var data = new Object();
    data.newsType = this.data.newsType;
    data.teamId = globalCache.getCurrentTeamId();
    data.page = this.data.page;
    data.rows = this.data.rows;
    var that = this;
    httpUtil.requestGet("newsList", data, function (res, that) {
      if (that.data.page == 1) {
        that.setData({
          newsList: []
        })
      }

      if (res.data.length > 0) {
        var newsList = that.data.newsList.concat(res.data);
        that.setData({
          hasNews: true,
          newsList: newsList
        })
      }
      
      if (that.data.newsList.length <= 0) {
        that.setData({
          hasNews: false,
        })
      }
    }, that, "加载中...");
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.autoRefresh) {
      this.setData({
        autoRefresh: false
      })
      wx.startPullDownRefresh();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      page: 1
    })
    this.getNewsList();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '玩命加载中',
    })
    var page = this.data.page + 1;
    this.setData({
      page: page
    })
    this.getNewsList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 删除新闻
   */
  deleteNews: function (event) {
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          var newsId = event.currentTarget.dataset.id;
          var index = event.currentTarget.dataset.index;
          httpUtil.requestPost("deleteNews", { newsId: newsId }, function (res, that) {
            var newsList = that.data.newsList;
            newsList.splice(index, 1);
            var hasNews = true;
            if (newsList.length <= 0) {
              hasNews = false;
            }
            that.setData({
              newsList: newsList,
              hasNews: hasNews
            });

            wx.showToast({
              title: '成功删除',
            });
          }, that, "提交中...");
        }
      }
    })
  },

  setTouchMove: function (e) {
    if (e.touches[0].clientX < this.data.showWidth && e.touches[0].clientY < this.data.showHeight 
        && e.touches[0].clientX > 0 && e.touches[0].clientY > 0) {
      this.setData({
        left: e.touches[0].clientX,
        top: e.touches[0].clientY
      })
    } else {
      this.setData({
        left: this.data.showWidth,
        top: this.data.showHeight
      })
    }
  },

  /**
   * 增加新闻
   */
  addNews: function () {
    this.setData({
      autoRefresh: true
    })
    wx.navigateTo({
      url: '../addNews/addNews?newsType=' + this.data.newsType
    })
  },

  /**
   * 点击放大图片
   */
  enlargeImage: function (event) {
    var src = event.currentTarget.dataset.src;
    var index = event.currentTarget.dataset.index;
    wx.previewImage({
      current: src,
      urls: this.data.newsList[index].imageUrlList
    })
  },
})