// pages/score/score.js
const httpUtil = require("../../utils/httpUtil.js");
const globalCache = require("../../utils/cache.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    rows: 10,
    scoreList: [],
    hasScoreData: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var childName = globalCache.getCurrentChildName();
    wx.setNavigationBarTitle({
      title: childName + '的成绩',
    });

    this.scoreList();
  },

  scoreList: function () {
    var that = this;
    var data = new Object();
    data.page = that.data.page;
    data.rows = that.data.rows;
    data.childId = globalCache.getCurrentChildId();
    httpUtil.requestGet("childScoreList", data, function (res, that) {
      if (that.data.page == 1) {
        that.setData({
          scoreList: []
        })
      }

      if (res.data.length > 0) {
        var scoreList = that.data.scoreList.concat(res.data);
        that.setData({
          scoreList: scoreList,
          hasScoreData: true,
        })
      }

      if (that.data.scoreList.length <= 0) {
        that.setData({
          hasScoreData: false,
        })
      }

    }, that, "加载中...");
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      page: 1
    })
    this.scoreList();
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // wx.showLoading({
    //   title: '玩命加载中',
    // })
    var page = this.data.page + 1;
    this.setData({
      page: page
    })
    this.scoreList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})