// pages/homework/homework.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    top: 0,
    left: 0,
    showHeight: 0,
    showWidth: 0,
    windowHeight: 0,
    windowWidth: 0,
    imageStyle: 'single-news-image',
    defaultUrl: '../../images/ic_default_avatar.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '家庭作业'
    });

    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          top: res.windowHeight - 150,
          left: res.windowWidth - 100,
          showHeight: res.windowHeight - 150,
          showWidth: res.windowWidth - 100,
          windowHeight: res.windowHeight,
          windowWidth: res.windowWidth,
          imageStyle: 'news-image'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  deleteNews: function (event) {
    var newsId = event.currentTarget.dataset['id'];
    console.log("------------>" + newsId)
  },

  setTouchMove: function (e) {
    if (e.touches[0].clientX < this.data.showWidth && e.touches[0].clientY < this.data.showHeight
      && e.touches[0].clientX > 0 && e.touches[0].clientY > 0) {
      this.setData({
        left: e.touches[0].clientX,
        top: e.touches[0].clientY
      })
    } else {
      this.setData({
        left: this.data.showWidth,
        top: this.data.showHeight
      })
    }
  },

  addNews: function () {
    console.log("---------------> add");
    wx.showToast({
      title: '加载中',
    })
  }
})